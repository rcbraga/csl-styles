CSL Style Repository
====================

[bitbucket.org/rcbraga/csl-styles/](https://bitbucket.org/rcbraga/csl-styles/) is the Rodolpho C. Braga  and other LabMol's members repository for Citation Style Language (CSL) styles and is maintained by [LabMol Research Group](http://labmol.farmacia.ufg.br).

Licensing
---------

All the styles in this repository are released under the [Creative Commons Attribution-ShareAlike 3.0 Unported license](http://creativecommons.org/licenses/by-sa/3.0/).
For attribution, any software using CSL styles from this repository must include a clear mention of the CSL project and a link to [CitationStyles.org](http://citationstyles.org/). When distributing these styles, the listings of authors and contributors in the style metadata must be kept as is.